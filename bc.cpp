/*
 * File:   BarcodeDecode.cpp
 * Author: skg
 *
 * Created on 26 August, 2015, 3:57 PM
 */

#include <iostream>

#include "bc.h"
#include <DMPro_Types.h>

namespace vendor_dmatrix {

BarcodeDecode::BarcodeDecode() {}

BarcodeDecode::BarcodeDecode(int cols, int rows) {
  maxrowQ = rows;
  maxcolQ = cols;
  pmembits = (TRow)malloc(maxrowQ * maxcolQ);     //  Image in Memory
  pbits = (TRow *)malloc(maxrowQ * sizeof(TRow)); // pointers to ScanLines
}

BarcodeDecode::BarcodeDecode(const BarcodeDecode &orig) {}

BarcodeDecode::~BarcodeDecode() {
  // free(pmembits);
  // free(pbits);
}

int BarcodeDecode::decode(unsigned char *image) {
  for (row = 0; row < maxrowQ; row++) {
    pbits[row] = &image[maxcolQ * row];
  }
  welldec = 0;
  // create Data Matrix decoder
  PDM_Decoder pDecoder = Connect_DM_Decoder(maxrowQ, maxcolQ);
  TDM_OptMode opt;
  if (pDecoder) {
    // Set decoder options

    // opt.maxDMCount =  1;
    opt.maxDMCount = 100;

    // opt.cellColor  = CL_BLACKONWHITE;
    // opt.cellColor  = CL_WHITEONBLACK;
    opt.cellColor = CL_ANY;

    // opt.mirrorMode = MM_NORMAL;
    // opt.mirrorMode = MM_MIRROR;
    opt.mirrorMode = MM_ANY;

    // opt.speedMode    = SP_FAST;
    opt.speedMode = SP_ACCURATEPLUS;

    opt.qualityMask = DM_QM_ALL;
    // opt.qualityMask = DM_QM_NO;

    // opt.labelMode = LM_STANDARD;   // set to decode STANDARD matrix
    opt.labelMode = LM_ST_DOT;
    // opt.labelMode = LM_DOTPEEN;  // set to decode DOTPEEN matrix
    // opt.labelMode = LM_FAX;      // set to decode FAX matrix
    opt.timeOut = 30;
    // opt.filterMode = FM_NON;
    opt.filterMode = 2;
    opt.qzMode = DMQZ_SMALL; // DMQZ_NORMAL;

    PDM_Options dec1 = Create_DM_Options(pDecoder, opt);

    fn = 0;
    sumtime = 0;

    rowcount = maxrowQ;
    colcount = maxcolQ;

    beg = clock();
    // decode
    result = DecodeDM_Bits(dec1, rowcount, colcount, pbits);
    end = clock();

    // printf("\n result = %4d\n ",result);
    // std::cout << "Time taken Barcode Decode (ms): " << (end-beg)*0.001 <<
    // std::endl;
    TDM_ImageInfo *pdm_imageinfo = GetDM_ImageInfo(dec1);
    dmq = pdm_imageinfo->DMCount;
    // printf("\n DM count = %4d\n",dmq);
    rr = pdm_imageinfo->RejectionReason;

    if (dmq > 0 && rr == DM_RR_OK) {
      for (i = 0; i <= dmq - 1; i++) {
        PDM_Info pdm_info = GetDM_Info(dec1, i);
        // std::cout << "Barcode Detected: " << pdm_info->pch << std::endl;

        Barcode *_barcode =
            new Barcode(reinterpret_cast<char *>(pdm_info->pch));

        _barcode->addBarcodeCorners(pdm_info->rowcols);
        barcodes.push_back(_barcode);
      }
      return dmq;
    }
  }
  return 0;
}

void BarcodeDecode::clear() { this->barcodes.clear(); }

Barcode::Barcode(std::string barcodeString) { barcodeData = barcodeString; }

Barcode::Barcode(const Barcode &orig) {}

Barcode::~Barcode() {}

cv::Point2f Barcode::getCenterBarcode(void) {
  cv::Point2f centerPoint = cv::Point2f(0.0f, 0.0f);
  for (size_t i = 0; i < cornerPoints.size(); i++) {
    centerPoint = centerPoint + cornerPoints[i];
  }

  centerPoint.x = centerPoint.x / 4;
  centerPoint.y = centerPoint.y / 4;

  return centerPoint;
}

cv::Rect Barcode::getBoundingRectBarcode(int cols, int rows) {
  float xSmallest = INT_MAX;
  float ySmallest = INT_MAX;

  float xLargest = 0;
  float yLargest = 0;

  for (size_t i = 0; i < cornerPoints.size(); i++) {
    if (cornerPoints[i].x < xSmallest)
      xSmallest = cornerPoints[i].x;

    if (cornerPoints[i].y < ySmallest)
      ySmallest = cornerPoints[i].y;

    if (cornerPoints[i].x > xLargest)
      xLargest = cornerPoints[i].x;

    if (cornerPoints[i].y > yLargest)
      yLargest = cornerPoints[i].y;
  }

  // std::cout << "Points X: " << points[n] << "\tY: " << points[n+1] <<
  // std::endl;

  int length = std::max(xLargest - xSmallest, yLargest - ySmallest);

  cv::Point startRectPoint = cv::Point(
      (xSmallest - 0.5 * length) < 0 ? 0 : (xSmallest - 0.5 * length),
      (ySmallest - 0.5 * length) < 0 ? 0 : (ySmallest - 0.5 * length));

  cv::Point endRectPoint = cv::Point(
      (xLargest + 0.5 * length) > cols ? cols : (xLargest + 0.5 * length),
      (yLargest + 0.5 * length) > rows ? rows : (yLargest + 0.5 * length));

  cv::Rect roi = cv::Rect(startRectPoint, endRectPoint);

  return roi;
}

void Barcode::addBarcodeCorners(float *points) {
  for (int n = 0; n < 8; n += 2)
    cornerPoints.push_back(cv::Point2f(points[n + 1], points[n]));

  free(points);
}
} // namespace vendor_dmatrix
