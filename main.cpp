#include <iostream>
#include <spdlog/spdlog.h>
#include <thread>
#include <zmq.hpp>

#include "bc.h"

std::string loggerName = "Worker";

void create_worker(std::string portPush, std::string portPull) {
  int width = 320;
  int height = 256;
  vendor_dmatrix::BarcodeDecode *barcodeDecode =
      new vendor_dmatrix::BarcodeDecode(width, height);
  zmq::context_t context(1);
  //  Socket to receive messages on
  zmq::socket_t receiver(context, ZMQ_PULL);
  receiver.connect(portPull);

  //  Socket to send messages to
  zmq::socket_t sender(context, ZMQ_PUSH);
  sender.bind(portPush);
  auto logger = spdlog::get(loggerName);

  // socket.bind("tcp://*:5556");
  //  socket.connect(port);
  //  socket.setsockopt(ZMQ_SUBSCRIBE, "B", 1);
  //  Process messages from receiver and controller

  while (true) {
    logger->debug() << "Waiting for messages";
    zmq::message_t reply;
    zmq::pollitem_t items[] = {{receiver, 0, ZMQ_POLLIN, 0}};
    // zmq::poll(&items[0], 2, -1);
    // if (items[0].revents & ZMQ_POLLIN) {

    //  Wait for next request from client
    receiver.recv(&reply);
    cv::Mat image(height, width, CV_8UC1, reply.data());
    // std::cout << "Received Hello" << std::endl;

    logger->debug() << "Received image";
    barcodeDecode->decode(image.data);
    if (barcodeDecode->barcodes.size() > 0) {
      logger->debug() << "Barcode: " << barcodeDecode->barcodes[0]->barcodeData;
      std::string bc = barcodeDecode->barcodes[0]->barcodeData;
      zmq::message_t request(bc.length());
      memcpy(request.data(), bc.c_str(), bc.length());
      sender.send(request);
    } else {
      zmq::message_t request(2);
      memcpy(request.data(), "No", 2);
      sender.send(request);
      logger->warn() << "Barcode not found";
    }
    barcodeDecode->clear();
    //}
    // Send reply back to client
  }
}

int main() {
  int queue_size = 1048576;
  spdlog::set_async_mode(queue_size,
                         spdlog::async_overflow_policy::discard_log_msg);
  auto logger = spdlog::rotating_logger_mt(
      loggerName, "/data/fw/log/ButlerIPUWorker", 1024 * 1024 * 10, 5, true);
  logger->set_pattern("[%Y-%d-%m %T.%f] [%l] [%n] [thread %t] %v");
  logger->set_level(spdlog::level::debug);
  logger->info() << "Logger Created";
  spdlog::get(loggerName)->info() << "Logger Created";

  std::string worker_thread_push_ports[] = {"tcp://*:5551", "tcp://*:5553",
                                            "tcp://*:5555"};
  std::string worker_thread_pull_ports[] = {
      "tcp://localhost:5552", "tcp://localhost:5554", "tcp://localhost:5556"};

  std::thread worker1(create_worker, worker_thread_push_ports[0],
                      worker_thread_pull_ports[0]);
  std::thread worker2(create_worker, worker_thread_push_ports[1],
                      worker_thread_pull_ports[1]);
  std::thread worker3(create_worker, worker_thread_push_ports[2],
                      worker_thread_pull_ports[2]);
  worker1.join();
  worker2.join();
  worker3.join();
  return 0;
}
