/*
 * File:   BarcodeDecode.h
 * Author: skg
 *
 * Created on 26 August, 2015, 3:57 PM
 */

#ifndef BARCODEDECODE_H
#define BARCODEDECODE_H

#include <opencv2/core/core.hpp>
#include <string>
#include <time.h>
#include <vector>
namespace vendor_dmatrix {
class Barcode {

public:
  std::vector<cv::Point2f> cornerPoints;
  std::string barcodeData;

  Barcode(std::string barcodeString);
  Barcode(const Barcode &orig);
  virtual ~Barcode();

  cv::Point2f getCenterBarcode(void);
  cv::Rect getBoundingRectBarcode(int rows, int cols);
  void addBarcodeCorners(float *points);
};


class BarcodeDecode {
public:
  BarcodeDecode();
  BarcodeDecode(int cols, int rows);
  BarcodeDecode(const BarcodeDecode &orig);
  int decode(unsigned char *image);
  void clear();
  virtual ~BarcodeDecode();
  std::vector<Barcode *> barcodes;

private:
  unsigned char **pbits;
  unsigned char *pmembits; // Image in Memory
  int ResLoadBMP;
  int rowcount, colcount, row, result;
  int i;
  int k;
  int rr;
  clock_t beg, end;
  int fn, sumtime;
  int dmq;
  int welldec;

  int maxrowQ; // maximum of image rows
  int maxcolQ;
};
} // namespace vendor_2dtg
#endif /* BARCODEDECODE_H */
